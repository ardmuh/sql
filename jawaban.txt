Soal 1 Membuat Database
    CREATE DATABASE myshop;

Soal 2 Membuat Table di dalam Database
a. Tabel users
    CREATE TABLE users(
	id 	INT(8) AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255),
    email VARCHAR(255),
    password VARCHAR(255)
    );

b. Tabel categories
    CREATE TABLE categories(
	id 	INT(8) AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255)
    );

c. Tabel items
    CREATE TABLE items(
	id 	INT(8) AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255),
    description VARCHAR(255),
    price int,
    stock int,
    category_id int,
    FOREIGN KEY (category_id) REFERENCES categories(id)
    );

Soal 3 Memasukan Data pada Tabel
a. Tabel users
    INSERT INTO users(name, email, password) VALUES("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe.com", "jenita123");

b. Tabel categories
    INSERT INTO categories(name) VALUES("gadget"),("cloth"),("men"),("women"),("branded");

c. Tabel items
    INSERT INTO items(name, description, price, stock, category_id) VALUES("Sumsang b50", "hape keren dari merek Sumsang", 4000000, 100, 1),("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2), ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);


Soal 4 Mengambil Data dari Database
a. Mengambil data users
Buatlah sebuah query untuk mendapatkan data seluruh user pada table users. Sajikan semua field pada table users KECUALI password nya.
    SELECT id, name, email FROM users;

b. Mengambil data items
- Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).
    SELECT * FROM items WHERE price > 1000000;

- Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja).
    SELECT * FROM items WHERE name LIKE "%sang%";

c. Menampilkan data items join dengan kategori
Buatlah sebuah query untuk menampilkan data items yang dilengkapi dengan data nama kategori di masing-masing items (gunakan join).
    SELECT i.name, i.description, i.price, i.stock, i.category_id, c.name AS kategori FROM items as i LEFT JOIN categories as c ON i.category_id=c.id;

Soal 5 Mengubah Data dari Database
Ubahlah data pada table items untuk item dengan nama sumsang b50 harganya (price) menjadi 2500000. Masukkan query pada text jawaban di nomor ke 5
    UPDATE items SET price = 2500000 WHERE name like "%b50";

    
